from django.urls import path

from .views import HomePageView
from .views import LoginPageView
from .views import ForgotPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('login/', LoginPageView.as_view(), name='login'),
    path('forgot/', ForgotPageView.as_view(), name='forgot'),
]