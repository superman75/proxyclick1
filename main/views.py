from django.views.generic import TemplateView

class HomePageView(TemplateView):
	template_name = 'home.html'


class LoginPageView(TemplateView):
	 template_name = 'login.html'

class ForgotPageView(TemplateView):
	template_name = 'forgot.html'